# pw-testing

Temporary repo for testing pipewire features

### Build
```
mkdir _build
gcc -Wall mod-pwmon.c -o _build/mod-pwmon $(pkg-config --cflags --libs libpipewire-0.3)
```

### Run
```
./_build/mod-pwmon
```