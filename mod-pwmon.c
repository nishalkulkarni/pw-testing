#include <getopt.h>
#include <signal.h>
#include <stdio.h>

#include <spa/debug/format.h>
#include <spa/debug/pod.h>
#include <spa/debug/types.h>
#include <spa/utils/result.h>
#include <spa/utils/string.h>

#include <pipewire/pipewire.h>

struct data
{
  struct pw_main_loop *loop;
  struct pw_context *context;

  struct pw_core *core;

  struct pw_registry *registry;
  struct spa_hook registry_listener;
};

struct proxy_data
{
  struct data *data;
  bool first;
  struct pw_proxy *proxy;
  uint32_t id;
  uint32_t permissions;
  uint32_t version;
  char *type;
  void *info;
  pw_destroy_t destroy;
  struct spa_hook proxy_listener;
  struct spa_hook object_listener;
};

static void
print_properties (const struct spa_dict *props, char mark)
{
  const struct spa_dict_item *item;

  printf ("%c\tproperties:\n", mark);
  if (props == NULL || props->n_items == 0)
    {
      printf ("\t\tnone\n");
      return;
    }

  spa_dict_for_each (item, props)
  {
    if (item->value)
      printf ("%c\t\t%s = \"%s\"\n", mark, item->key, item->value);
    else
      printf ("%c\t\t%s = (null)\n", mark, item->key);
  }
}

#define MARK_CHANGE(f)                                                        \
  ((print_mark && ((info)->change_mask & (f))) ? '*' : ' ')

static void
print_node (struct proxy_data *data)
{
  struct pw_node_info *info = data->info;
  bool print_mark;

  if (data->first)
    {
      printf ("added:\n");
      print_mark = false;
      data->first = false;
    }
  else
    {
      printf ("changed:\n");
      print_mark = true;
    }

  printf ("\tid: %d\n", data->id);
  printf ("\ttype: %s (version %d)\n", data->type, data->version);
  printf ("%c\tstate: \"%s\"", MARK_CHANGE (PW_NODE_CHANGE_MASK_STATE),
          pw_node_state_as_string (info->state));
  if (info->state == PW_NODE_STATE_ERROR && info->error)
    printf (" \"%s\"\n", info->error);
  else
    printf ("\n");
  print_properties (info->props, MARK_CHANGE (PW_NODE_CHANGE_MASK_PROPS));
}

static void
node_event_info (void *object, const struct pw_node_info *info)
{
  struct proxy_data *data = object;

  info = data->info = pw_node_info_update (data->info, info);

  print_node (data);
}

static const struct pw_node_events node_events
    = { PW_VERSION_NODE_EVENTS, .info = node_event_info };

static void
removed_proxy (void *data)
{
  struct proxy_data *pd = data;
  pw_proxy_destroy (pd->proxy);
}

static void
destroy_proxy (void *data)
{
  struct proxy_data *pd = data;

  if (pd->info == NULL)
    return;

  if (pd->destroy)
    pd->destroy (pd->info);
  pd->info = NULL;
  free (pd->type);
}

static const struct pw_proxy_events proxy_events = {
  PW_VERSION_PROXY_EVENTS,
  .removed = removed_proxy,
  .destroy = destroy_proxy,
};

static void
registry_event_global (void *data, uint32_t id, uint32_t permissions,
                       const char *type, uint32_t version,
                       const struct spa_dict *props)
{
  struct data *d = data;
  struct pw_proxy *proxy;
  struct proxy_data *pd;

  if (!spa_streq (type, PW_TYPE_INTERFACE_Node))
    return;

  proxy = pw_registry_bind (d->registry, id, type, PW_VERSION_NODE,
                            sizeof (struct proxy_data));
  if (proxy == NULL)
    {
      printf ("failed to create proxy");
      return;
    }

  pd = pw_proxy_get_user_data (proxy);
  pd->data = d;
  pd->first = true;
  pd->proxy = proxy;
  pd->id = id;
  pd->permissions = permissions;
  pd->version = version;
  pd->type = strdup (type);
  pd->destroy = (pw_destroy_t)pw_node_info_free;
  pw_proxy_add_object_listener (proxy, &pd->object_listener, &node_events, pd);
  pw_proxy_add_listener (proxy, &pd->proxy_listener, &proxy_events, pd);
}

static void
registry_event_global_remove (void *object, uint32_t id)
{
  printf ("removed:\tid: %u\n", id);
}

static const struct pw_registry_events registry_events = {
  PW_VERSION_REGISTRY_EVENTS,
  .global = registry_event_global,
  .global_remove = registry_event_global_remove,
};

static void
do_quit (void *data, int signal_number)
{
  struct data *d = data;
  pw_main_loop_quit (d->loop);
}

int
main (int argc, char *argv[])
{
  struct data data = { 0 };
  struct pw_loop *l;
  const char *opt_remote = NULL;

  pw_init (&argc, &argv);

  data.loop = pw_main_loop_new (NULL);
  if (data.loop == NULL)
    {
      fprintf (stderr, "can't create main loop: %m\n");
      return -1;
    }

  l = pw_main_loop_get_loop (data.loop);
  pw_loop_add_signal (l, SIGINT, do_quit, &data);
  pw_loop_add_signal (l, SIGTERM, do_quit, &data);

  data.context = pw_context_new (l, NULL, 0);
  if (data.context == NULL)
    {
      fprintf (stderr, "can't create context: %m\n");
      return -1;
    }

  data.core = pw_context_connect (
      data.context, pw_properties_new (PW_KEY_REMOTE_NAME, opt_remote, NULL),
      0);
  if (data.core == NULL)
    {
      fprintf (stderr, "can't connect: %m\n");
      return -1;
    }

  data.registry = pw_core_get_registry (data.core, PW_VERSION_REGISTRY, 0);
  pw_registry_add_listener (data.registry, &data.registry_listener,
                            &registry_events, &data);

  pw_main_loop_run (data.loop);

  pw_proxy_destroy ((struct pw_proxy *)data.registry);
  pw_context_destroy (data.context);
  pw_main_loop_destroy (data.loop);
  pw_deinit ();

  return 0;
}